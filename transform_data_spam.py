import pandas as pd


df = pd.read_csv("smsspam.csv",sep="\t",skipinitialspace=True)
df.Classification.map(lambda wat:  1 if wat == "ham" else 0)

# Nombre total de mots distincts :
len(set(df.Text.map(lambda text: text+" ").sum().split()))
